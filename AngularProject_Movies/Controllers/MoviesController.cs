﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AngularProject_Movies.Models;
using Newtonsoft.Json;

namespace AngularProject_Movies.Controllers
{
    public class MoviesController : Controller
    {

        public ActionResult List()
        {
            return View();
        }
        public JsonResult YTSList(string Page)
        {

            movieEntities db = new movieEntities();
            int pageNo = int.Parse(Page);
            Table product = db.Tables.Find(pageNo - 1);
            string pureJson = product.jsonString;

            return Json(pureJson, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOMDBMovieDetails(string ImdbID)
        {
            string OMDBApiUrl = @"http://www.omdbapi.com/?i=" + ImdbID + "&plot=full&r=json&tomatoes=true";
            string url = OMDBApiUrl;
            string jsonRes = WebAPI.GetJSONString(url);
            return Json(jsonRes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetYTSMovieDetails(string ID)
        {
            string YTSApiDetails = @"https://yts.to/api/v2/movie_details.json?movie_id=" + ID;
            string url = YTSApiDetails;
            string jsonRes = WebAPI.GetJSONString(url);
            return Json(jsonRes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMovieReviews(string ID)
        {
            string YTSApiDetails = @"https://yts.to/api/v2/movie_reviews.json?movie_id=" + ID;
            string url = YTSApiDetails;
            string jsonRes = WebAPI.GetJSONString(url);
            return Json(jsonRes, JsonRequestBehavior.AllowGet);
        }

        public string GetMovies(string SetNo)
        {
            string YtsAPI_ListBaseStr1 = @"https://yts.to/api/v2/list_movies.json?limit=15&page=";
            string url = YtsAPI_ListBaseStr1 + SetNo;
            string jsonRes = WebAPI.GetJSONString(url);
            return jsonRes;
        }

        public JsonResult GetRottenUpcoming()
        {
            string apiKet = "nc9k5hntfps2jcndbk56vp4s";
            string RottenTomatoUrl = @"http://api.rottentomatoes.com/api/public/v1.0/lists/movies/upcoming.json?page_limit=20&page=1&apikey="+apiKet;
            string jsonRes = WebAPI.GetJSONString(RottenTomatoUrl);
            
            return Json(jsonRes, JsonRequestBehavior.AllowGet);
        }
    }
}
