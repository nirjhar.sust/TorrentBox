﻿using AngularProject_Movies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AngularProject_Movies.Controllers
{
    public class TrendingController : Controller
    {
        //
        // GET: /Trending/

        public ActionResult List()
        {
            return View();
        }

        public JsonResult TrendingNow()
        {
            UpcomingMovies p = new UpcomingMovies();
            string ll = p.GetMovies();
            return Json(ll, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOMDBData(string IMDBID)
        {
            OMDBData p = new OMDBData();
            p.IMDBID = IMDBID;
            OMDBMovieObj ll = p.GetMovies();
            return Json(ll, JsonRequestBehavior.AllowGet);
        }

    }

}
