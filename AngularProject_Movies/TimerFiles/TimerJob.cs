﻿using AngularProject_Movies.Models;
using Newtonsoft.Json;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AngularProject_Movies.TimerFiles
{
    public class TimerJob : IJob
    {
        private int PageNo;
        private const string YtsAPI_ListBaseStr1 = @"https://yts.to/api/v2/list_movies.json?limit=50&page=";

        public void Execute(IJobExecutionContext context)
        {

            //string resultList = GetMovies("1");
            //RootObject rootOBJ = new RootObject();
            //var results = JsonConvert.DeserializeObject<dynamic>(resultList);
            //rootOBJ = results.ToObject<RootObject>();
            movieEntities db = new movieEntities();
            for (int i=0; i<10; i++)
            {
                Table product = db.Tables.Find(i);
                int countpage = i + 1;
                product.jsonString = GetMovies(countpage.ToString());
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
            }

        }
        public string GetMovies(string SetNo)
        {
            string url = YtsAPI_ListBaseStr1+SetNo;
            string jsonRes = WebAPI.GetJSONString(url);
            return jsonRes;
        }
    }
}