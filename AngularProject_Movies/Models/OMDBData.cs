﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularProject_Movies.Models
{
    public class OMDBData
    {
        public string IMDBID="";
        private const string OMDBAPI_Base1 = @"http://www.omdbapi.com/?i=";
        private const string OMDBAPI_Base2 = "&plot=full&r=json&tomatoes=true";

        public OMDBMovieObj GetMovies()
        {
            if (IMDBID != "")
            {
                string Url = OMDBAPI_Base1 + IMDBID + OMDBAPI_Base2;
                string jsonRes = WebAPI.GetJSONString(Url);

                OMDBMovieObj singlemovie = new OMDBMovieObj();
                var omdbresults = JsonConvert.DeserializeObject<dynamic>(jsonRes);
                singlemovie = omdbresults.ToObject<OMDBMovieObj>();

                return singlemovie;
            }
            else { return new OMDBMovieObj(); }
        }
    }
}