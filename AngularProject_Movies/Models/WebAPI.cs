﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace AngularProject_Movies.Models
{
    public class WebAPI
    {
        public static string GetJSONString(string requestPath)
        {
            WebClient client = new WebClient();
            string htmlCode = client.DownloadString(requestPath);

            return htmlCode;
        }
    }
}