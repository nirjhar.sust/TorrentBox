﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace AngularProject_Movies.Models
{
    public class UpcomingMovies
    {
        private const string YtsAPI_Base = @"https://yts.to/api/upcoming.json";

        public string GetMovies()
        {
            string jsonRes = WebAPI.GetJSONString(YtsAPI_Base);
            return jsonRes;
        }

    }
}
