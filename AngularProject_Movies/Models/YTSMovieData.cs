﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularProject_Movies.Models
{
    public class YTSMovieData
    {
        public string YtsID = "";
        private const string YTSMovieDetail = @"https://yts.to/api/v2/movie_details.json?movie_id=";

        public SingleMovieDetail GetMovies()
        {
            if (YtsID != "")
            {
                string Url = YTSMovieDetail + YtsID;
                string jsonRes = WebAPI.GetJSONString(Url);

                SingleMovieDetail singlemovie = new SingleMovieDetail();
                var omdbresults = JsonConvert.DeserializeObject<dynamic>(jsonRes);
                singlemovie = omdbresults.data.ToObject<SingleMovieDetail>();

                return singlemovie;
            }
            else { return new SingleMovieDetail(); }
        }
    }
}