﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularProject_Movies.Models
{
    public class YTSMovieOBJ
    {
        public string status { get; set; }
        public string status_message { get; set; }
        public SingleMovieDetail data { get; set; }
        public Meta metatag { get; set; }
    }


    public class SingleMovieDetail
    {
        public int id { get; set; }
        public string url { get; set; }
        public string imdb_code { get; set; }
        public string title { get; set; }
        public string title_long { get; set; }
        public int year { get; set; }
        public double rating { get; set; }
        public int runtime { get; set; }
        public List<string> genres { get; set; }
        public string language { get; set; }
        public string mpa_rating { get; set; }
        public int download_count { get; set; }
        public int like_count { get; set; }
        public int rt_critics_score { get; set; }
        public string rt_critics_rating { get; set; }
        public int rt_audience_score { get; set; }
        public string rt_audience_rating { get; set; }
        public string description_intro { get; set; }
        public string description_full { get; set; }
        public string yt_trailer_code { get; set; }
        public string date_uploaded { get; set; }
        public int date_uploaded_unix { get; set; }
        public List<Torrent> torrents { get; set; }
    }


}