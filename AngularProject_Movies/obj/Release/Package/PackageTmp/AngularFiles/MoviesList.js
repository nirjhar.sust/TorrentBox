﻿var app = angular.module('ListMoviesModule', []);

app.controller('ListMoviesCtrl', ['$http', '$scope','$location', '$anchorScroll', function ($http, $scope,$location,$anchorScroll) {

    $scope.LoadData = function () {

        $scope.wait = true;
        $http.get('/Movies/YTSList?Page=' + $scope.page).success(function (data) {
            $scope.ListAll = angular.fromJson(data);

            $scope.wait = false;


        }).error(function (data) {
            $scope.ListAll = angular.fromJson(data);
        });
    };

    $scope.GetUpComingMovies = function () {

        var urlDetail = "/Movies/GetRottenUpcoming";
        $http.get(urlDetail).success(function (data) {
            $scope.UpComingTomatoes = angular.fromJson(data);

        }).error(function (data) {
            $scope.UpComingTomatoes = angular.fromJson(data);
        });
    };

    $scope.ScrollToTop = function () {

        $location.hash('top');
        $anchorScroll();
    };


}]);

app.controller('MovieDetail', ['$http', '$scope', '$location', '$anchorScroll', function ($http, $scope, $location, $anchorScroll) {

     $scope.LoadDetailOMDB = function (id) {

        var urlDetail = "/Movies/GetOMDBMovieDetails?ImdbID="+id;

        $http.get(urlDetail).success(function (data) {
            $scope.MovieDetailsOMDB = angular.fromJson(data);

        }).error(function (data) {
            $scope.MovieDetailsOMDB = angular.fromJson(data);
        });

    };

    $scope.LoadDetailYTS = function (id) {


        var urlDetail = "/Movies/GetYTSMovieDetails?ID=" + id;
        $http.get(urlDetail).success(function (data) {
            $scope.MovieDetailsYTS = angular.fromJson(data);

        }).error(function (data) {
            $scope.MovieDetailsYTS = angular.fromJson(data);
        });
    };



    $scope.ShowDiv = function (DIVid) {

        if ($('#object-' + DIVid).is(":visible") == true)
        {
            $('#button-' + DIVid).text('View Details');
            $('#object-' + DIVid).hide();
        }
        else if ($('#object-' + DIVid).is(":visible") == false)
        {
            $('#button-' + DIVid).text('Hide Details');
            $('#object-' + DIVid).show();
        }
    };

    $scope.GetVideoUrl = function (YoutubeID) {

        $('#video-' + YoutubeID).html('<iframe src="http://www.youtube.com/embed/'+YoutubeID+'" width="320" height="240" frameborder="0" allowfullscreen="true">');
        $('#video-' + YoutubeID + 'thumb').hide();

    };

    $scope.GetMovieReview = function (id) {

        $('#loadrev' + id).show();
        var urlDetail = "/Movies/GetMovieReviews?ID=" + id;
        $http.get(urlDetail).success(function (data) {
            $scope.MovieReviewYTS = angular.fromJson(data);
            $('#loadrev' + id).hide();

        }).error(function (data) {
            $scope.MovieReviewYTS = angular.fromJson(data);
        });
    };

}]);
