﻿/// <reference path="../Views/Partial.html" />

var app = angular.module('UpcomingMoviesModule', []);

app.controller('UpComingMoviesCtrl', ['$http', '$scope', '$rootScope', '$location', '$anchorScroll', function ($http, $scope, $rootScope, $location, $anchorScroll) {
    
    $scope.LoadData = function () {

        $scope.MovieList = [];
        $scope.wait = true;
        $http.get('/Trending/TrendingNow/').success(function (data) {
            $scope.MovieList = angular.fromJson(data);

            var getPlotFunc = function (index) {

                var movieID = $scope.MovieList[index].ImdbCode;
                $http.get('/Trending/GetOMDBData?IMDBID=' + movieID).success(function (data) {

                    var pureJson = angular.fromJson(data);
                    $scope.MovieList[index].Plot = pureJson.Plot;
                    $scope.MovieList[index].Released = pureJson.Released;
                    $scope.MovieList[index].Genre = pureJson.Genre;
                    $scope.MovieList[index].Director = pureJson.Director;
                    $scope.MovieList[index].imdbRating = pureJson.imdbRating;
                    $scope.MovieList[index].imdbVotes = pureJson.imdbVotes;

                }).error(function (data) {


                });
            };
            for (var i = 0; i < $scope.MovieList.length; i++) {
                getPlotFunc(i);
            }
            $scope.wait = false;


        }).error(function (data) {
            $scope.MovieList = angular.fromJson(data);
        });
    };

    
}]);
